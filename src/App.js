
import './App.css';
import { BrowserRouter, Route, Routes } from "react-router-dom"
import Header from "./components/Header/Header"
import Bikes from "./components/Bikes/Bikes"


const App = () => (
  <BrowserRouter>
    <Routes>
      <Route path='/' exact element={<Header>
      </Header>}>
      </Route>
      <Route path='/Bikes' exact element={<Bikes></Bikes>} />
    </Routes>
  </BrowserRouter>
)

export default App;
