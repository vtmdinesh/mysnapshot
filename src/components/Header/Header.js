import React, { useState, useEffect } from 'react'
import { NavLink } from 'react-router-dom';
import { apiKey } from "../../api/config";
import Container from "../Container/Container"
import Noimage from '../Noimage/Noimage';
import Spinner from "../Spinner/Spinner"
import "./header.css"



const Header = () => {
    const [searchInput, setSearchInput] = useState('')
    const [loading, setLoading] = useState(false)
    const [imageList, setImageList] = useState([])

    const runSearch = (searchInput) => {
        console.log({ searchInput })
        fetch(
            `https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=${apiKey}&tags=${searchInput}&per_page=24&format=json&nojsoncallback=1`
        )
            .then(response => {

                return response?.json()

            }).then(data => {
                setLoading(false)
                setImageList(data?.photos?.photo)
            })
            .catch(error => {
                setLoading(false)
                console.log(
                    "Encountered an error with fetching and parsing data",
                    error
                );

            });
    }

    const getSearchInput = (event) => {

        //console.log(event.keyCode)
        // this.runSearch(event.target)
        // this.setState({ searchInput: event.target.value })

        if (event.keyCode === 13) {
            runSearch(event.target.value)
            setSearchInput(event.target.value)

        }
    };

    useEffect(() => {
        setLoading(true)
        runSearch(searchInput)
    }, [searchInput])

    useEffect(() => {
        setLoading(true)
        runSearch('nature')
    }, [])




    return (
        <div className="header-container">
            <h1 className="app-title">SnapShot</h1>
            <input type="search" className="input" id="search" onKeyDown={getSearchInput} placeholder='Search here...' />
            <nav className="main-nav">
                <div className="nav-list-item"><NavLink to="/Bikes">Bikes</NavLink>
                </div>
                <div className="nav-list-item"><NavLink to="Houses">Houses</NavLink></div>
                <div className="nav-list-item"><NavLink to="food">Food</NavLink></div>

            </nav>
            {loading ? <Spinner /> : imageList?.length ?
                <Container searchInput={searchInput} imageList={imageList} /> : <Noimage />

            }
        </div>

    )
}




export default Header