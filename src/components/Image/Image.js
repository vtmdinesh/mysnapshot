import React from "react";
import "./image.css"

const Image = ({ url, title }) => (
    <div className="img-container" >
        <img className="image" src={url} alt={title} />
    </div>


);

export default Image;
