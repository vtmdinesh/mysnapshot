import React, { useState } from 'react'
import Noimage from '../Noimage/Noimage';
import Image from "../Image/Image"
import "./container.css"
import { Dialog, Box, Grid } from '@mui/material';

// function PaperComponent(props) {
//     return (
//         <Draggable
//             handle="#draggable-dialog-title"
//             cancel={'[class*="MuiDialogContent-root"]'}
//         >
//             <Paper {...props} />
//         </Draggable>
//     );
// }

const Container = (props) => {
    let { searchInput, imageList } = props
    const [isOpen, setIsOpen] = useState(false)
    const [selectedImage, setSelectedImage] = useState([])
    let images;
    let noImages


    const handleClickOpen = (e) => {
        setIsOpen(true);
        setSelectedImage(e.target.src)
    };

    const handleClose = () => {
        setIsOpen(false);
    };

    // if (imageList && imageList.length > 0) {
    //     //console.log(imageLisst)
    //     images = imageList.map(image => {
    //         let farm = image.farm;
    //         let server = image.server;
    //         let id = image.id;
    //         let secret = image.secret;
    //         let title = image.title;
    //         let url = `https://farm${farm}.staticflickr.com/${server}/${id}_${secret}_m.jpg`;
    //         return <Image url={url} key={id} alt={title} />;

    //     })
    // }
    // else {
    //     return noImages = <Noimage />
    // }
    const newFn = () => {
        console.log('in')
    }
    return (
        <div>
            <div className="container">
                <p className="title">{searchInput} <span className="title">Images</span></p>
                {imageList?.length > 0 ?
                    // <div className="image-container">
                    <Grid container spacing={3} className='cont'>
                        {imageList.map(image => {
                            let farm = image.farm;
                            let server = image.server;
                            let id = image.id;
                            let secret = image.secret;
                            let title = image.title;
                            let url = `https://farm${farm}.staticflickr.com/${server}/${id}_${secret}_m.jpg`;
                            return (
                                <Grid item xs={12} sm={6} md={3}>
                                    <img className="image" src={url} alt={title} onClick={(e) => handleClickOpen(e)} />
                                </Grid>
                            )
                        })}
                    </Grid>
                    // </div> 
                    :
                    <div><Noimage /></div>}

            </div>
            <Dialog
                open={isOpen}
                onClose={handleClose}
                // PaperComponent={PaperComponent}
                aria-labelledby="draggable-dialog-title"
                sx={{ width: '100%', height: '100%' }}
            >
                <Box style={{ padding: '20px', height: '600px', width: '400px' }}>
                    <img className="fullImage" src={selectedImage} alt={'image'} />

                </Box>
            </Dialog>
        </div >
    );


}

export default Container