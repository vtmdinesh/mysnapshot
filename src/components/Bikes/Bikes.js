import React from 'react'
import { apiKey } from "../../api/config";
import "./header.css"
import Container from '../Container/Container';


let Bikes = () => {


    let runSearch = (bike) => {
        fetch
            (
                `https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=${apiKey}&tags=${bike}&per_page=24&format=json&nojsoncallback=1`
            )
            .then(response => {
                return response.json()

            }).then((data) => {

                return (
                    <Container searchInput="Bike" imageList={data.photos.photo} />
                )
            })
            .catch(error => {
                console.log(
                    "Encountered an error with fetching and parsing data",
                    error
                );

            });
    }
    runSearch("bike")
}


export default Bikes